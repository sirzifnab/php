<?php
$formaction = $_SERVER['PHP_SELF'];
$qfxfile = array_key_exists("qfxfile", $_POST)?$_POST["qfxfile"]:0;
?>
<!DOCTYPE html>
<html>
<head>
<style>
table,td { border:1px solid black; }
td:first-child { background-color:silver; color:navy; font-weight:bold;}
.tblprop { font-family:Arial; }
.tblhead { text-align:center; color:gold; background-color: black;}
</style>
</head>
<body>
<div id="formbox">
	<form action='<?=$formaction?>' method='POST' name='qfxform'>
		<input type="hidden" name="MAX_FILE_SIZE" value="100000" />
		<input 	name='qfxfile' 
				type='file' 
				placeholder="Select QFX/OFX file" 
				accept=".qfx,.ofx" 
				onchange="document.qfxform.submit();"
		>
	</form>
</div>
<div id="qfx_data">
<?php

if ($qfxfile) { //"ofx37694.qfx"
	$qfx=objectifyQFX($qfxfile);
	show("QFX",$qfx);
}


function objectifyQFX($qfx_file) {
	$qfx=$key=[]; $oid=0;$oid_del="#!#";
	$getstrpos = function(string $s, string $c): int { return strpos(' '.$s.' ',$c); };
	foreach (file($qfx_file) as $qfx_line) { 
		$qfx_line=rtrim($qfx_line);	$len = strlen($qfx_line);  $property=$value=""; 	
		[$hdr, $start, $close, $end] = array_map($getstrpos, array_fill(0,4,$qfx_line), 
		[ ":",    "<",    ">", "</"]);		
		if ($hdr and !$start) { [$property, $value] = explode(":", $qfx_line); }
		if (($start and $close) and !$end) { 
			$property = substr($qfx_line, $start, $close-$start-1); 
			if ($len>$close) { $value = substr($qfx_line, $close); }
			else  			 { $oid+=1; array_push($key, $property.$oid_del.$oid); } 	
		} 
		if ($end) { 
			if ($end==$start and $key) { array_pop($key); }
			if ($start and $end>$start) { 
				$property=substr($qfx_line, $start, $close-$start-1); 
				$value = substr($qfx_line, $close, $end -1); 
			}			
		}	
		if (strlen($value)) { 
			if (!$key) { $qfx[$property]= $value; }
			else { 	
				if (array_key_exists(0,$key)) { if (!array_key_exists($key[0], $qfx)) 												
						 { $qfx[$key[0]] =[]; }
				if (array_key_exists(1,$key)) { if (!array_key_exists($key[1], $qfx[$key[0]])) 										
						 { $qfx[$key[0]][$key[1]] =[]; }
				if (array_key_exists(2,$key)) { if (!array_key_exists($key[2], $qfx[$key[0]][$key[1]])) 							
						 { $qfx[$key[0]][$key[1]][$key[2]] =[]; }
				if (array_key_exists(3,$key)) { if (!array_key_exists($key[3], $qfx[$key[0]][$key[1]][$key[2]])) 					
						 { $qfx[$key[0]][$key[1]][$key[2]][$key[3]] =[]; }
				if (array_key_exists(4,$key)) { if (!array_key_exists($key[4], $qfx[$key[0]][$key[1]][$key[2]][$key[3]])) 			
						 { $qfx[$key[0]][$key[1]][$key[2]][$key[3]][$key[4]]=[]; }
				if (array_key_exists(5,$key)) { if (!array_key_exists($key[5], $qfx[$key[0]][$key[1]][$key[2]][$key[3]][$key[4]])) 	
						 { $qfx[$key[0]][$key[1]][$key[2]][$key[3]][$key[4]][$key[5]] =[]; }
						   $qfx[$key[0]][$key[1]][$key[2]][$key[3]][$key[4]][$key[5]]	[$property] = $value; 
				}	else { $qfx[$key[0]][$key[1]][$key[2]][$key[3]][$key[4]]			[$property] = $value; }
				} 	else { $qfx[$key[0]][$key[1]][$key[2]][$key[3]]						[$property] = $value; } 
				}	else { $qfx[$key[0]][$key[1]][$key[2]]								[$property] = $value; }
				}	else { $qfx[$key[0]][$key[1]]										[$property] = $value; }
				}	else { $qfx[$key[0]]												[$property] = $value; }
				} 
				
/*
				$getobjkey = function($o,$i): string { return ($o)?$o:""; };
				[$_1, $_2, $_3, $_4, $_5, $_6] = array_map($getobjkey, $key, array(0,1,2,3,4,5));
				if ($_1) 	 { if (!array_key_exists($_1, $qfx))  						   { $qfx[$_1] = []; }
					if ($_2) { if (!array_key_exists($_2, $qfx[$_1]))     				   { $qfx[$_1][$_2]=[]; }
					if ($_3) { if (!array_key_exists($_3, $qfx[$_1][$_2])) 	 			   { $qfx[$_1][$_2][$_3]=[]; }
					if ($_4) { if (!array_key_exists($_4, $qfx[$_1][$_2][$_3])) 	 	   { $qfx[$_1][$_2][$_3][$_4]=[]; }
					if ($_5) { if (!array_key_exists($_5, $qfx[$_1][$_2][$_3][$_4]))       { $qfx[$_1][$_2][$_3][$_4][$_5]=[]; }
					if ($_6) { if (!array_key_exists($_6, $qfx[$_1][$_2][$_3][$_4][$_5]))  { $qfx[$_1][$_2][$_3][$_4][$_5][$_6] = []; }
							   $qfx[$_1][$_2][$_3][$_4][$_5][$_6][$property] = $value; 
					}	else { $qfx[$_1][$_2][$_3][$_4][$_5]	 [$property] = $value; }
					} 	else { $qfx[$_1][$_2][$_3][$_4]			 [$property] = $value; } 
					}	else { $qfx[$_1][$_2][$_3]				 [$property] = $value; }
					}	else { $qfx[$_1][$_2]					 [$property] = $value; }
					}	else { $qfx[$_1]						 [$property] = $value; }
				}
*/
	} 	}	}	//echo json_encode($qfx);
	return $qfx;
}

function show($t,$d) { 
	echo "<h2><a name='".$t."'>".substr($t,0,strpos($t,'#'))."</a></h2>"; 
	echo tableize($d); 
	foreach($d as $k=>$v) 
	{ if (gettype($v)=="array") { show($k,$v); } }
}

function tableize($data) { $output="\n<table>"; 
	$hdr=0;
	$lastobjtype="";
	foreach($data as $rowkey=>$row) {
		if (gettype($row)=="string") { $output.="<tr class='tblprop'><th>".$rowkey."</th><td>".$row."</td></tr>"; }
		if (gettype($row)=="array") {			
			if (substr($rowkey, 0, strpos($rowkey,"#"))!=$lastobjtype) { 
				//var_dump($data); echo "<br>";
				$row_html="<tr class='tblhead'><td></td>\n";				
				foreach($row as $datakey=>$data) { $row_html.="<td>".((gettype($data)=="string")?$datakey:"(object)")."</td>"; }
				$row_html.="</tr>\n";
				$hdr=1;
			}else $row_html="";		
			$row_html.="<tr><td><a href='#".$rowkey."'>".$rowkey."</td>";
			foreach($row as $datakey=>$data) { $row_html.="<td>".((gettype($data)=="array")?"<a href='#".$datakey."'>".$datakey."</a>":$data)."</td>"; } 	
			$row_html.="</tr>\n";
			$output.=$row_html;
		}	$lastkey=substr($rowkey, 0, strpos($rowkey,"#"));
	} 	$output.="</table>\n";
	return $output; 
}

function Example() {
	echo $qfx["OFXHEADER"]."<br>";
	echo $qfx["DATA"]."<br>";
	echo $qfx["VERSION"]."<br>";
	echo $qfx["SECURITY"]."<br>";
	echo $qfx["ENCODING"]."<br>";
	echo $qfx["CHARSET"]."<br>";
	echo $qfx["COMPRESSION"]."<br>";
	echo $qfx["OLDFILEUID"]."<br>";
	echo $qfx["NEWFILEUID"]."<br>";

	$trans=$qfx["OFX#!#1"]["BANKMSGSRSV1#!#5"]["STMTTRNRS#!#6"]["STMTRS#!#8"]["BANKTRANLIST#!#10"];
	$stmt_tbl= tableize($qfx["OFX#!#1"]["BANKMSGSRSV1#!#5"]["STMTTRNRS#!#6"]["STMTRS#!#8"]["BANKTRANLIST#!#10"]);
	echo $stmt_tbl;
} // Example();
?>
</div>
</body>
</html>